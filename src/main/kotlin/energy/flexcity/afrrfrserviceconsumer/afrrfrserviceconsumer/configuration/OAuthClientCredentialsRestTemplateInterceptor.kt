package energy.flexcity.afrrfrserviceconsumer.afrrfrserviceconsumer.configuration

import org.springframework.http.HttpHeaders
import org.springframework.http.client.ClientHttpRequestExecution
import org.springframework.http.client.ClientHttpRequestInterceptor
import org.springframework.http.client.ClientHttpResponse
import org.springframework.security.core.Authentication
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.oauth2.client.OAuth2AuthorizeRequest
import org.springframework.security.oauth2.client.ReactiveOAuth2AuthorizedClientManager
import org.springframework.security.oauth2.client.registration.ClientRegistration
import java.util.Objects.isNull


class OAuthClientCredentialsRestTemplateInterceptor(
    private val manager: ReactiveOAuth2AuthorizedClientManager,
    private val clientRegistration: ClientRegistration,
) : ClientHttpRequestInterceptor {
    private val principal: Authentication

    companion object {
        val BEARER_PREFIX = "Bearer "
    }

    init {
        this.principal = createPrincipal()
    }

    private fun createPrincipal(): Authentication {
        return object : Authentication {
            override fun getAuthorities() = emptySet<GrantedAuthority>()

            override fun getCredentials(): Any? = null

            override fun getDetails(): Any? = null

            override fun getPrincipal(): Any = this

            override fun isAuthenticated() = false

            override fun setAuthenticated(isAuthenticated: Boolean) {}

            override fun getName(): String = clientRegistration.clientId
        }
    }

    override fun intercept(
        request: org.springframework.http.HttpRequest,
        body: ByteArray,
        execution: ClientHttpRequestExecution
    ): ClientHttpResponse {
        val oAuth2AuthorizeRequest: OAuth2AuthorizeRequest = OAuth2AuthorizeRequest
            .withClientRegistrationId(clientRegistration.registrationId)
            .principal(principal)
            .build()
        val client = manager.authorize(oAuth2AuthorizeRequest).block()
            ?: throw IllegalStateException("client credentials flow on " + clientRegistration.registrationId + " failed, client is null")
        request.headers.add(HttpHeaders.AUTHORIZATION, BEARER_PREFIX + client.accessToken.tokenValue)
        return execution.execute(request, body)
    }
}