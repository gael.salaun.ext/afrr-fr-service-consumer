package energy.flexcity.afrrfrserviceconsumer.afrrfrserviceconsumer.configuration

import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.oauth2.client.*
import org.springframework.security.oauth2.client.registration.ReactiveClientRegistrationRepository
import org.springframework.security.oauth2.client.web.reactive.function.client.ServerOAuth2AuthorizedClientExchangeFilterFunction
import org.springframework.web.client.RestClient
import org.springframework.web.client.RestTemplate
import org.springframework.web.reactive.function.client.WebClient


@Configuration
class RestClientsConfiguration {

    // Reference for code and explanations : https://www.baeldung.com/spring-webclient-oauth2

    companion object {
        val AFRR_FR_SERVICE_CLIENT_REGISTRATION_ID = "afrr-fr-service"
    }

    @Bean
    fun webClient(
        clientRegistrations: ReactiveClientRegistrationRepository,
        authorizedClientManager : ReactiveOAuth2AuthorizedClientManager
    ): WebClient {
        val oauth = ServerOAuth2AuthorizedClientExchangeFilterFunction(authorizedClientManager)
        oauth.setDefaultClientRegistrationId(AFRR_FR_SERVICE_CLIENT_REGISTRATION_ID)
        return WebClient.builder()
            .filter(oauth)
            .build()
    }

    @Bean
    fun authorizedClientManager(clientRegistrations: ReactiveClientRegistrationRepository): AuthorizedClientServiceReactiveOAuth2AuthorizedClientManager {
        val clientService = InMemoryReactiveOAuth2AuthorizedClientService(clientRegistrations)
        val authorizedClientManager =
            AuthorizedClientServiceReactiveOAuth2AuthorizedClientManager(clientRegistrations, clientService)
        return authorizedClientManager
    }

    @Bean
    fun restTemplate(
        restTemplateBuilder: RestTemplateBuilder,
        clientRegistrations: ReactiveClientRegistrationRepository,
        authorizedClientManager : ReactiveOAuth2AuthorizedClientManager
    ): RestTemplate {
        val clientRegistration = clientRegistrations.findByRegistrationId(AFRR_FR_SERVICE_CLIENT_REGISTRATION_ID).block() ?: throw IllegalStateException("No client found")
        return restTemplateBuilder
            .additionalInterceptors(
                OAuthClientCredentialsRestTemplateInterceptor(
                    authorizedClientManager,
                    clientRegistration
                )
            )
            .build()
    }

    @Bean
    fun restClient(
        restTemplateBuilder: RestTemplateBuilder,
        clientRegistrations: ReactiveClientRegistrationRepository,
        authorizedClientManager : ReactiveOAuth2AuthorizedClientManager
    ): RestClient {
        val clientRegistration = clientRegistrations.findByRegistrationId(AFRR_FR_SERVICE_CLIENT_REGISTRATION_ID).block() ?: throw IllegalStateException("No client found")
        return RestClient.builder()
            .requestInterceptor(OAuthClientCredentialsRestTemplateInterceptor(
                authorizedClientManager,
                clientRegistration
            )).build()
    }
}