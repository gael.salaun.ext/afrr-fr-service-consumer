package energy.flexcity.afrrfrserviceconsumer.afrrfrserviceconsumer.service

import energy.flexcity.afrrfrserviceconsumer.afrrfrserviceconsumer.dto.ActivationInfoDto

interface ActivationService {
    fun findActivationsInfoByWebclient(): ActivationInfoDto?

    fun findActivationsInfoByRestTemplate(): ActivationInfoDto?

    fun findActivationsInfoByRestClient(): ActivationInfoDto?

}