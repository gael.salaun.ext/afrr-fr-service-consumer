package energy.flexcity.afrrfrserviceconsumer.afrrfrserviceconsumer.service

import energy.flexcity.afrrfrserviceconsumer.afrrfrserviceconsumer.dto.ActivationInfoDto
import org.springframework.stereotype.Service
import org.springframework.web.client.RestClient
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.getForEntity
import org.springframework.web.reactive.function.client.WebClient

@Service
class ActivationServiceImpl(
    private val webClient: WebClient,
    private val restTemplate: RestTemplate,
    private val restClient: RestClient
) : ActivationService {

    companion object {
        const val ACTIVATIONS_INFO_URL = "http://localhost:8000/api/v1/activations"
    }

    override fun findActivationsInfoByWebclient() =
        webClient.get()
            .uri(ACTIVATIONS_INFO_URL)
            .retrieve()
            .bodyToMono(ActivationInfoDto::class.java)
            .block()

    override fun findActivationsInfoByRestTemplate() =
        restTemplate.getForEntity<ActivationInfoDto>(ACTIVATIONS_INFO_URL).body

    override fun findActivationsInfoByRestClient() =
        restClient.get().uri(ACTIVATIONS_INFO_URL).retrieve().body(ActivationInfoDto::class.java)

}