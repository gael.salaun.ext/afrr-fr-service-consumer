package energy.flexcity.afrrfrserviceconsumer.afrrfrserviceconsumer

import energy.flexcity.afrrfrserviceconsumer.afrrfrserviceconsumer.service.ActivationService
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication


@SpringBootApplication
class AfrrFrServiceConsumerApplication(val activationService: ActivationService) : CommandLineRunner{

    override fun run(vararg args: String) {
        println("Found activationsInfo by WebClient : ${activationService.findActivationsInfoByWebclient()}")
        println("Found activationsInfo by RestTemplate : ${activationService.findActivationsInfoByRestTemplate()}")
        println("Found activationsInfo by RestClient : ${activationService.findActivationsInfoByRestClient()}")
    }
}

fun main(args: Array<String>) {
    runApplication<AfrrFrServiceConsumerApplication>(*args)
}
