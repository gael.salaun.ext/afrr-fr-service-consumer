package energy.flexcity.afrrfrserviceconsumer.afrrfrserviceconsumer.dto

data class ActivationInfoDto(val info:List<String>)